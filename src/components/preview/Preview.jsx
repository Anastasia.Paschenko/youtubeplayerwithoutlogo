import { PlayBtn } from '../common/Buttons';
import './styles.scss';

export const Preview = ({ videoId }) => {
    return (
      <div className='preview'>
        <PlayBtn className='preview-button' />
        <div className='preview-blackout' />
        <img className='preview-thumbnail' src={`https://img.youtube.com/vi/${videoId}/maxresdefault.jpg`} />
      </div>
    );
  }