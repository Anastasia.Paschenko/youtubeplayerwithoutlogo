import YouTube from 'react-youtube';
import './App.scss';
import { useState } from 'react';
import { Preview } from './components/preview/Preview';
import { useMediaQuery } from 'react-responsive'
import cn from 'classnames';

const MUSIC = {
  id: "90yVsVJZ36A",
  isVertical: false,
}
const DOG = {
  id: "wNqZM7e0_18",
  isVertical: false,
}
const CAT_SHORT = {
  id: "b0pSIP2eJvs",
  isVertical: true,
}
const PRIVATE_VIDEO = {
  id: "vcFgdYftmCU",
  isVertical: true,
}
const VIDEOS = [PRIVATE_VIDEO, DOG, MUSIC, CAT_SHORT];

const opts = {
  playerVars: {
    // https://developers.google.com/youtube/player_parameters
    controls: 0,
    mute: 1,
    rel: 0,
    enablejsapi: 1,
    playsinline: 1,
    fullscreen: 1,
    origin: 'http://localhost:3000/'
  },
}

function App() {
  const [videoStatuses, setVideoStatuses] = useState(new Map());
  const [videos, setVideos] = useState(new Map());
  const [isPlayShown, setIsPlayShown] = useState(true);
  const [fullScreenVideoId, setFullScreenVideoId] = useState('')
  const [isFullScreenShown, setIsFullScreenShown] = useState(true)

  const togglePause = (v) => {
    if (!videos.has(v)) return

    const isPlayed = videoStatuses.has(v) ? !videoStatuses.get(v) : true
    setVideoStatuses(new Map(videoStatuses.set(v, isPlayed)))
    isPlayed ? videos.get(v).playVideo() : videos.get(v).pauseVideo()
  };

  const onReady = (v) => (event) => {
    setVideos(new Map(videos.set(v, event.target)))
  };

  const onEnd = (v) => {
    const isPlayed = videoStatuses.has(v) ? !videoStatuses.get(v) : true
    setVideoStatuses(new Map(videoStatuses.set(v, isPlayed)))
  };

  const isMobile = useMediaQuery({
    query: '(max-width: 800px)'
  })

  return (
    <>
      <div className={cn('body', { 'body-fullscreen': fullScreenVideoId !== '' })}>
        <div className={cn('content', { mobile: isMobile })}>
          <div className='button-group'>
            <button
              className='button-primary'
              onClick={() => setIsPlayShown(p => !p)}>
              {isPlayShown ? 'Hide' : 'Show'} custom preview
            </button>
            <button
              className='button-primary'
              onClick={() => setIsFullScreenShown(p => !p)}>
              {isFullScreenShown ? 'Hide' : 'Show'} fullscreen button
            </button>
          </div>
          {VIDEOS.map(v => (
            <>
              <div className={cn({ 'fullscreen': fullScreenVideoId == v.id })}>
                <div key={v.id}
                  className={cn('videoContainer',
                    { 'videoContainer-fullscreen': fullScreenVideoId == v.id },
                    { 'videoContainer-fullscreen-vertical': fullScreenVideoId == v.id && v.isVertical }
                  )}
                  onClick={() => togglePause(v.id)}
                >
                  {!videoStatuses.get(v.id) && isPlayShown && <Preview videoId={v.id} />}
                  <YouTube videoId={v.id} opts={opts} onReady={onReady(v.id)} onEnd={() => onEnd(v.id)} />
                </div>
              </div>
              {isFullScreenShown && (fullScreenVideoId === ''
                ? <button className='button-primary' onClick={() => setFullScreenVideoId(v.id)}>Show fullscreen</button>
                : <button className={cn('button-primary', 'exit-fullscreen')} onClick={() => setFullScreenVideoId('')}>Exit fullscreen mode</button>
              )}
            </>
          ))}
        </div>
      </div>
    </>
  );
}

export default App;
